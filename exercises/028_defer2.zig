//
// Now that you know how "defer" works, let's do something more
// interesting with it.
//
const std = @import("std");

pub fn main() void {
    const animals = [_]u8{ 'g', 'c', 'd', 'd', 'g', 'z' };

    for (animals) |a| printAnimal(a);

    std.debug.print("done.\n", .{});
}

// This function is _supposed_ to print an animal name in parentheses
// like "(Goat) ", but we somehow need to print the end parenthesis
// even though this function can return in four different places!
fn printAnimal(animal: u8) void {
    std.debug.print("(", .{});

<<<<<<< Updated upstream
    defer std.debug.print(") ", .{}); // <---- how?!
||||||| constructed merge base
    std.debug.print(") ", .{}); // <---- how?!
=======
    var token: []const u8 = undefined;
>>>>>>> Stashed changes

    defer std.debug.print("{s}) ", .{token}); // <---- how?!

    token = switch (animal) {
        'g' => "Goat",
        'c' => "Cat",
        'd' => "Dog",
        else => "Unknown",
    };
}
