//
//    "Elephants walking
//     Along the trails
//
//     Are holding hands
//     By holding tails."
//
//     from Holding Hands
//       by Lenore M. Link
//
const std = @import("std");

const Elephant = struct {
    letter: u8,
    tail: ?*Elephant = null,
    visited: bool = false,
};

pub fn main() !void {
    var elephantA = Elephant{ .letter = 'A' };
    var elephantB = Elephant{ .letter = 'B' };
    // (Please add Elephant B here!)
    var elephantC = Elephant{ .letter = 'C' };

    // Link the elephants so that each tail "points" to the next elephant.
    // They make a circle: A->B->C->A...
    elephantA.tail = &elephantB;
    elephantB.tail = &elephantC;
    // (Please link Elephant B's tail to Elephant C here!)
    elephantC.tail = &elephantA;

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    try visitElephants(allocator, &elephantA);

    std.debug.print("\n", .{});
}

// This function visits all elephants once, starting with the
// first elephant and following the tails to the next elephant.
// If we did not "mark" the elephants as visited (by setting
// visited=true), then this would loop infinitely!
fn visitElephants(allocator: std.mem.Allocator, first_elephant: *Elephant) !void {
    var visited = std.AutoHashMap(Elephant, void).init(allocator);
    defer visited.deinit();

    var e = first_elephant.*;

    while (true) {
        if (visited.get(e)) |_| break;

        std.debug.print("Elephant {u}. ", .{e.letter});
        try visited.put(e, {});

        if (e.tail) |f| e = f.* else break;
    }
}
